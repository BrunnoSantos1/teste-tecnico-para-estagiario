#### Teste Técnico estágiario!

#### Descrição da tarefa:
- Desenvolver código que calcule o IMC - índice de massa corporal de cada pessoa: IMC = massa/altura2
- Formatar e imprimir o nome completo de cada pessoa com seu IMC ao lado direito   
- O nome completo deve ser escrito com todas as letras maiúsculas    
- O IMC deve estar separado do nome completo por um espaço em branco   
- O IMC deve estar com duas casas decimais, usando a vírgula como separador   

#### Tecnologias utilizadas:
- Editor: Vscode
- Linguagem: Python (Apenas utilizado libs internas: "CSV","Math")

#### Funcionamento:
- O sistema captura dados do DATASET.CSV
- Calcula IMC e formata a saída
- Gera um arquivo resultado-DATASET.CSV com os resultados
