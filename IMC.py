import csv
import math

lista_IMC = []
coluna_Nome = []
coluna_Sobrenome = []
coluna_Massa = []
coluna_Altura = []
lista_Pessoas = []

class Pessoa:
    def __init__(self,nome,sobrenome,imc):
        self.nome = nome
        self.sobrenome = sobrenome
        self.imc = imc

    def __repr__(self) -> str:
        return "{} {} {}\n".format(self.nome,self.sobrenome,self.imc)

with open('DATASET.CSV', 'r') as data:
    data_IMC = csv.reader(data,delimiter=";")
    cabecalho = next(data_IMC)

    for line in data_IMC:
        coluna_Nome = line[0].split()
        for i in coluna_Nome:
         coluna_Nome = i.upper()

        coluna_Sobrenome = line[1].split("\n")
        for i in coluna_Sobrenome:
            coluna_Sobrenome = i.split()
            coluna_Sobrenome = " ".join(coluna_Sobrenome)
            coluna_Sobrenome = coluna_Sobrenome.upper()

        coluna_Massa = line[2].split()
        for i in coluna_Massa:
            coluna_Massa = float(i.replace(",","."))

        coluna_Altura = line[3].split()
        for i in coluna_Altura:
            coluna_Altura = float(i.replace(",","."))
            coluna_Altura = math.pow(coluna_Altura,2)
            lista_IMC = str(round(coluna_Massa/coluna_Altura,2))
            lista_IMC = lista_IMC.replace(".",",")
            pessoa = Pessoa(coluna_Nome,coluna_Sobrenome,lista_IMC)
            lista_Pessoas.append(pessoa)

resultado = open("resultado-DATASET.CSV", 'w', newline='')
resultado_csv = csv.writer(resultado)
r_Cabecalho = ["Nome Sobrenome IMC"]
resultado_csv.writerow(r_Cabecalho)
resultado_csv.writerow(lista_Pessoas)